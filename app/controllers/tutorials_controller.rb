class TutorialsController < ApplicationController
  def new
    @tutorial = Tutorial.new
  end

  def edit
    @tutorial = Tutorial.find(params[:id])
  end
  
  def index
    @tutorials = Tutorial.all
  end

  def show
    @tutorial = Tutorial.find(params[:id])
  end

  def create
    @tutorial = Tutorial.new(tutorial_params)
    if @tutorial.save
      redirect_to tutorials_path
    else
      render :new
    end
  end

  def destroy
    @tutorial = Tutorial.find(params[:id])
    @tutorial.delete
    redirect_to tutorials_path
  end

  private

  def tutorial_params
    params.required(:tutorial).permit(:name, :description, :url, :provider_id)
  end
end

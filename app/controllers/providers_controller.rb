class ProvidersController < ApplicationController
  def index
    @providers = Provider.all
  end

  def show
    @provider = Provider.find(params[:id])
  end

  def new
    @provider = Provider.new
  end

  def create
    provider = Provider.create(provider_params)
    provider.save
    redirect_to providers_path
  end

  protected

  def provider_params
    params.required(:provider).permit(:name, :description, :url, :img)
  end
end

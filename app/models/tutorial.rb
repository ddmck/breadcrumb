class Tutorial
  include Mongoid::Document
  field :url, type: String
  field :name, type: String
  field :description, type: String
  # field :provider_id, type: ObjectId


  belongs_to :provider
  validates_uniqueness_of :url
  validates_presence_of :name, :provider_id
end

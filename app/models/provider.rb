class Provider
  include Mongoid::Document
  field :url, type: String
  field :name, type: String
  field :description, type: String
  field :img, type: String

  has_many :tutorials
end
